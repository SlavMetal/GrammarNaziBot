[![Requirements Status](https://requires.io/github/SlavMetal/GrammarNaziBot/requirements.svg?branch=master)](https://requires.io/github/SlavMetal/GrammarNaziBot/requirements/?branch=master)
# GrammarNaziBot
GrammarNaziBot is a Telegram bot that checks grammar of every message sent in private or group chat. Recognizes English, Russian and Ukrainian words using Yandex Speller API.
## Setup
1. Clone the repository:
    ```Bash
    git clone https://github.com/SlavMetal/GrammarNaziBot
    ```
1. Install required packages:
    ```Bash
    pip3 install -r requirements.txt
    ```
1. Replace sample data in config.yml with the real one: bot token from [@BotFather](https://t.me/BotFather), administrators IDs (required for some functions) and groups IDs where the bot will be used (or leave it blank to allow all groups). 
## Run
```Bash
python3 grammarnazibot.py
```
## Contribution
Any kind of help (pull requests, issues, etc.) would be very appreciated as this is my first Python program!
## Contact
You can contact me directly in [Telegram](https://t.me/SlavMetal).
